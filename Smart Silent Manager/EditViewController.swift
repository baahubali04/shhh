//
//  EditViewController.swift
//  Smart Silent Manager
//
//  Created by student on 3/10/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

import CoreData

class EditViewController: UIViewController {
    
    var event:Events?
    
    
    
    @IBOutlet weak var fromdate: UIDatePicker!
    
    @IBOutlet weak var todate: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        eventNameTF.text = event?.eventName
        fromdate.date = (event?.eventFrom)!
        todate.date = (event?.eventTo)!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBOutlet weak var eventNameTF: UITextField!
    
    @IBAction func saveClicked(_ sender: UIButton) {
        print("save clicked")
        do{
            let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let fetchRequest:NSFetchRequest<Events> = NSFetchRequest(entityName: "Events")
            let events = try moc.fetch(fetchRequest)
            for event in events {
                if event.index == AppDelegate.events.index! {
                    event.repeatingDays = AppDelegate.events.editEventRepeatDays.description
                    event.eventFrom = fromdate.date
                    event.eventTo = todate.date
                    try moc.save()
                    self.tabBarController?.selectedIndex = 2
                }
            }
            TriggerEvent().createEventTrigger()
        }catch{
            print("error \(error)")
        }
        
      
    }

}
