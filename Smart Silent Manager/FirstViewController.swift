//
//  FirstViewController.swift
//  Smart Silent Manager
//
//  Created by Team Bahubali on 2/16/18.
//  Copyright © 2018 Bahubali. All rights reserved.
//

import UIKit

import CoreData

// this class is used to handle description of events
class FirstViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    // this variable holds the managed object context
    let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // this function gets called when a view loads
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // this function will be invoked when ever a view appears, we will be fetching events from core data and storing them in events variable for future use and then reloads tableview data
    override func viewWillAppear(_ animated: Bool) {
        
        do {
            let fetchRequest:NSFetchRequest<Events> = NSFetchRequest(entityName: "Events")
            let events = try moc.fetch(fetchRequest)
            data = events
            tv?.reloadData()
            
        } catch {
            print("Error when trying to fetch: \(error)")
            
        }
        
        
        
    }
    
    // this variable holds the events
    var data:[Events]?
    
    // this variable holds the curent tableview
    var tv:UITableView?
    
    // This method gets called when tableview loads to get number of row in a section required to be presented in view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tv = tableView
        return data!.count
    }
    
    // This method gets called when tableview loads to get cell for each row by using events in data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
        cell.textLabel?.text = data![indexPath.row].eventName
        let event = data![indexPath.row]
        let calender = Calendar.current
        let date = calender.dateComponents([.hour,.minute ], from: event.eventFrom!)
        let dateto = calender.dateComponents([.hour,.minute ], from: event.eventTo!)
        cell.detailTextLabel?.text = "\(date.hour! % 12) : \(date.minute! % 60) \(date.hour! > 11 ? "AM" : "PM" ) - \(dateto.hour! % 12) : \(dateto.minute! % 60) \(date.hour! > 11 ? "AM" : "PM" )"
        return cell
    }
    
    // this function gets called when add button is clicked and changes presented view to add event view controller 
    @IBAction func goToAdd(_ sender: Any) {
       self.tabBarController!.selectedIndex = 1
        
    }
    
   


}

