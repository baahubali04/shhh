//
//  EventTableViewCell.swift
//  Smart Silent Manager
//
//  Created by student on 3/9/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit
import CoreData

class EventTableViewCell: UITableViewCell {
    
    var events:[Events] = []

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBOutlet weak var eventName: UILabel!
    
    @IBOutlet weak var eventfrom: UILabel!
    
    @IBOutlet weak var eventto: UILabel!
    
    @IBOutlet weak var editBTN: UIButton!
    
    @IBOutlet weak var deleteBTN: UIButton!
    
    @IBOutlet weak var monday: UIButton!
    
    @IBOutlet weak var tuesday: UIButton!
    
    @IBOutlet weak var wednesday: UIButton!
    
    @IBOutlet weak var thursday: UIButton!
    
    @IBOutlet weak var friday: UIButton!
    
    @IBOutlet weak var saturday: UIButton!
    
    @IBOutlet weak var sunday: UIButton!
    
}
