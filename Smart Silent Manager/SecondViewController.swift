    //
    //  SecondViewController.swift
    //  Smart Silent Manager
    //
    //  Created by student on 2/16/18.
    //  Copyright © 2018 student. All rights reserved.
    //

    import UIKit
    import CoreData
    
    // this class handles all the add event functionality
    class SecondViewController: UIViewController {
        
        // this variable holds the managed object context
        let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        // this method is used to load the repeating days and index of event
        override func viewDidLoad() {
            super.viewDidLoad()
            AppDelegate.events.index = AppDelegate.events.repeatDays.count
            AppDelegate.events.repeatDays.append(AppDelegate.events.addEventRepeatdays)
            
        }
        
        // this function get invoked when an edit button is clicked
        @IBAction func eventNameBTN(_ sender: UITextField) {
            self.view.endEditing(true)
        }
        
        // this is an outlet which refers to eventName
        @IBOutlet weak var eventName: UITextField!
        
        // this is an outlet which refers to date until which event has to performed
        @IBOutlet weak var toDate: UIDatePicker!
        
        // this is an outlet which refers to date from which event has to performed
        @IBOutlet weak var FromDate: UIDatePicker!
        
        // this method is invoked when an add event button is clicked and stores all the information into coredata
        @IBAction func AddEventClicked(_ sender: UIButton) {
           // reference for date comparision
    //        print(Calendar.current.compare(FromDate.date, to: Date(), toGranularity: .minute).rawValue)
    //        print(Calendar.current.component(.hour, from: FromDate.date))
    //        print(Calendar.current.component(.minute, from: FromDate.date))
    //        print(Calendar.current.component(.second, from: FromDate.date))
    //        print(Calendar.current.component(.hour, from: Date()))
    //        print(Calendar.current.component(.minute, from: Date()))
    //        print(Calendar.current.component(.second, from: Date()))
            
            
            let event = NSEntityDescription.insertNewObject(forEntityName: "Events",  into: moc) as! Events
            event.eventName = eventName.text
            event.eventFrom = FromDate.date
            event.eventTo = toDate.date
            event.repeatingDays = AppDelegate.events.addEventRepeatdays.description
            AppDelegate.events.index = AppDelegate.events.repeatDays.count
            AppDelegate.events.repeatDays.append(AppDelegate.events.addEventRepeatdays)
            do {
                try moc.save()
                AppDelegate.events.addEventRepeatdays = []
                self.tabBarController?.selectedIndex = 2
                TriggerEvent().createEventTrigger()
            } catch  {
                print("error while saving data")
            }
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
       
        // this method gets invoked when a segue is about happen, if the segue is  with identifier "AddEventToRepeatDays" then we will be set a variable in destination view controller
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            if segue.identifier == "AddEventToRepeatDays"{
                
                        print(AppDelegate.events.repeatDays.count - 1)
                        AppDelegate.events.index = AppDelegate.events.repeatDays.count - 1
                        let vc = segue.destination as! DayTableViewController
                        vc.isThisfromAddEventViewController = true
                
                    }
        }
        
        
      

    }

