//
//  DayTableViewController.swift
//  Smart Silent Manager
//
//  Created by Team bahubali on 3/9/18.
//  Copyright © 2018 Bahubali. All rights reserved.
//

import UIKit

import CoreData

class DayTableViewController: UITableViewController {
    
    // this variable stores the days need to be displayed in each cell of tableview
    var day = ["Every Monday","Every Tuesday","Every Wednesday","Every Thursday","Every Friday","Every Saturday","Every Sunday"]
    
    // this variable is used to know whether the segue is coming from add event viewcontroller or not
    var isThisfromAddEventViewController = false
    
     // this function gets called when a view loads
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // This method gets called when tableview loads to get number of sections required to be presented in view
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // This method gets called when tableview loads to get number of row in a section required to be presented in view
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return day.count
    }
    
    // this variable holds the managed object context
    let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // this method is invoked every time the view is about to appear, sets the repeatdays based on the segue is coming from the view
    override func viewWillAppear(_ animated: Bool) {
        
        if isThisfromAddEventViewController {
            repeatdays = AppDelegate.events.addEventRepeatdays
        }
        else {
            repeatdays = AppDelegate.events.editEventRepeatDays
            print(repeatdays)
        }
        
    }
    
    // this variable is used to store the repeatdays locally in this view controller
    var repeatdays:[Int] = []
    
    // this method is invoked while rendering each cell in tableview, checks event from segue source and checkmarks the days which are of that particular event
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "DayCell", for: indexPath)
        cell.textLabel?.text = day[indexPath.row]
        cell.textLabel?.textColor = UIColor.orange
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            for i in repeatdays {
                if i == indexPath.row {
                    cell.accessoryType = .checkmark
                }
            }
        return cell
    }
    
    // this method is invoked when a user select a cell,stores the selected cell in an repeat days array and checkmarks the cell
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        repeatdays.append(indexPath.row)

    }
    // this method is invoked when a user deselect a cell,removes the selected cell in an repeat days array and checkmarks
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .none
        var j = 0
        let loop = repeatdays
        for i in loop {
            if indexPath.row == i {
                repeatdays.remove(at: j)
                j -= 1
                print(repeatdays)
            }
            j += 1
        }
    }
    
    
    // this method is used to store state of the repeat days in edit and add event view controller whenever a view disappears
    override func viewWillDisappear(_ animated: Bool) {
        if(isThisfromAddEventViewController){
            AppDelegate.events.addEventRepeatdays = repeatdays
            isThisfromAddEventViewController = false
        }else{
            AppDelegate.events.editEventRepeatDays = repeatdays
            print(repeatdays)
            
        }
    }
 
  

}
