//
//  EventsTableViewController.swift
//  Smart Silent Manager
//
//  Created by Team Bahubali on 3/9/18.
//  Copyright © 2018 Bahubali. All rights reserved.
//

import UIKit

import CoreData


class EventsTableViewController: UITableViewController {
    
    
    // this variable holds the refreshed events when this view appears
    var events:[Events] = []
    
    // this variable holds the managed object context
    let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // this variable holds the selected event when the edit button is pressed
    var selectedEvent:Events?
    
    // this function gets called when a view loads
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // this variable holds the days for repeat days
     var day = ["Every Monday","Every Tuesday","Every Wednesday","Every Thursday","Every Friday","Every Saturday","Every Sunday"]
    
    // this function will be invoked when ever a view appears, we will be fetching events from core data and storing them in events variable for future use and then reloads data
    override func viewWillAppear(_ animated: Bool) {
        
        do {
            let fetchRequest:NSFetchRequest<Events> = NSFetchRequest(entityName: "Events")
            events = try moc.fetch(fetchRequest)
            
        } catch {
            print("Error when trying to fetch: \(error)")
            
        }
        tableView.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // This method gets called when tableview loads to get number of sections required to be presented in view
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // This method gets called when tableview loads to get number of row in a section required to be presented in view
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }

   
    // this methods renders the view for each cell in tableview assigns tags to edit and delete button in each cell and saves those tags to coredata
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventTableViewCell
        
        let repeatDays = events[indexPath.row].repeatingDays?.trimmingCharacters(in: CharacterSet(charactersIn: "[]")).components(separatedBy: ",")
        
        let dayBtnArray:[UIButton] = [cell.monday,cell.tuesday,cell.wednesday,cell.thursday,cell.friday,cell.saturday,cell.sunday]
        
        for btn in dayBtnArray {
            btn.backgroundColor = UIColor.black
        }
        
         for ele in repeatDays! {
            if ele != "" {
              dayBtnArray[Int(ele.trimmingCharacters(in: [" "]))!].backgroundColor = UIColor.white
            }
         }
        
        
        let df = DateFormatter()
        df.dateStyle = .medium
        df.formattingContext = .standalone
        df.timeStyle = .medium
        
        let calender = Calendar.current
        var date = calender.dateComponents([.hour,.minute ], from: events[indexPath.row].eventFrom!)
        cell.eventfrom.text = "\(String(format: "%2d", date.hour! % 12)) : \(date.minute! % 60) \(date.hour! > 11 ? "AM" : "PM" )"
        date = calender.dateComponents([.hour,.minute ], from: events[indexPath.row].eventTo!)
        cell.eventto.text = "\(date.hour! % 12) : \(date.minute! % 60) \(date.hour! > 11 ? "AM" : "PM" )"
        
        cell.eventName.text = events[indexPath.row].eventName
        
        cell.editBTN.tag = indexPath.row
        
        cell.deleteBTN.tag = indexPath.row
        
        events[indexPath.row].setValue(indexPath.row, forKey: "index")
        do {
            try moc.save()
        } catch  {
            print("error while saving moc in events table view controller")
        }
        
        return cell
    }
    // this method is invoked whenever we return back from edit view
    @IBAction func unwindFromEdit(segue:UIStoryboardSegue){
        
    }
    
    // this method is invoked whenever a row is selected to view the details of an event
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected index \(indexPath.row)")
    }
        // this methods is invoked whenever a edit button in a cell is clicked
        // gets all the events from managed object context and then updates the appropriate object and saves the managed object context
       @IBAction func editBTNClicked(_ sender: Any) {
                    let s = sender as? UIButton
        
                    AppDelegate.events.index = s!.tag

                    do {
                            let fetchRequest:NSFetchRequest<Events> = NSFetchRequest(entityName: "Events")
                            let events = try moc.fetch(fetchRequest)
                        var stuff:[Int] = []
                            for event in events {
                            if event.index == AppDelegate.events.index! {
                                selectedEvent = event
                            let repeatString = event.repeatingDays?.trimmingCharacters(in: CharacterSet(charactersIn: "[]")).components(separatedBy: ",")
                                    for ele in repeatString! {
                                            if ele != "" {
                                            stuff.append(Int(ele.trimmingCharacters(in: [" "]))!)
                                            }
                                        }
                                    }
                            
                                }
                        AppDelegate.events.editEventRepeatDays = stuff 
                        self.performSegue(withIdentifier: "EventTableToEditView", sender: self)
                        
                    
                    } catch {
                    print("Error when trying to fetch: \(error)")
                    
                    }
    
        }
    // this methods is invoked whenever a delete button in a cell is clicked
    // gets all the events from managed object context and then deletes the appropriate object and saves the managed object context
    @IBAction func deleteclicked(_ sender: UIButton) {
        
        do {
            let fetchRequest:NSFetchRequest<Events> = NSFetchRequest(entityName: "Events")
            let events = try moc.fetch(fetchRequest)
            for event in events {
                if Int(event.index) == sender.tag {
                     moc.delete(event)
                }
                
            }
            
            try moc.save()
            self.events = try moc.fetch(fetchRequest)
            tableView.reloadData()
            
        } catch {
            print("Error when trying to fetch: \(error)")
            
        }
    }
    
    // this method is invoked whenever a segue from this view is about to happen
    // passing event names to edit view so that selected event can be display in destination i.e, edit view controller

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! EditViewController
        vc.event = selectedEvent
      
        
        
    }
 

}
