#Smart Silent Manager

Smart Silent Manager app allows a user to set an event or activity which turns user phone automatically into silent mode based on the location and time.

##Features

- [X] Auto Silent mode on a week-to-week basis will help you to be more relaxed all through the year
- [X] Set unique silent period for each event
- [X] Turning user phone into silent mode based on the location

##Requirements

- iOS 11.2-

##Installation & Guide

Download the app and install app on to your mobile and follow below instructions.

![](home.png)   ![](add.png)   ![](events.png)

### Home Screen
Summary of current events added are displayed on the home screen. Home screen also contains option to add new events and navigate among other screens.

### Add Event
User can create a new event by entering the details required. If an event is repeating on a weekly basis, you set start and end dates for repeat intervel along with the intervel frequency.

### Event
User can edit an already created event or activity by clicking on the event either on Home Screen or Events Screen. An outdated or unrequired event can be removed from the Events screen.




